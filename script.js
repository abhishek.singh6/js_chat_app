document.getElementById('createChannel').addEventListener('submit', createChannel);
document.getElementById('createMessage').addEventListener('submit', createMessage);


var userName="random user";

function createNewUser() {
    var person = prompt("Please enter your name:", "");
   
        if(person == undefined || person==""){
            userName ="random user";
            console.log(userName)
        }
        else{
            userName = person;
        }

  }

//============SHOW CHANNEL=========
fetch('http://0.0.0.0:5000/channels/')
    .then(function(response) {
        return response.json();
    })
    .then(function(result){
        let dataArr = result.resources;
        let channel ="";
    
    for(let i =0; i<dataArr.length; i++){
        channel += `<li class="channelName" id =${dataArr[i].id} onclick="selectChannel(${dataArr[i].id})">#${dataArr[i].name}</li>`;
    }
    document.getElementById("channel").innerHTML = channel;
})



var channelId = {}
//===========SHOW MESSAGES==========
function selectChannel(id){

    channelName = document.getElementsByClassName("channelName");
  
    console.log(channelName[0].id);
    for(let i = 0; i<channelName.length; i++){
      console.log(channelName[i].id)
      document.getElementById(channelName[i].id).style.color = "#cbb3ce";
      document.getElementById(channelName[i].id).style.font = "bold";


    }    
    document.getElementById(id).style.color="white";


    console.log(id)
    channelId.id = id;
    let url ='http://0.0.0.0:5000'
    fetch(url+'/messages/?channel_id='+id)
    .then(function(response) {
        return response.json();
    })
    .then(function(result){
        // console.log(result)
        let dataArr = result.resources;
        let message ="";

        for(let i =0; i<dataArr.length; i++){
            message += `<div class="row" style="margin-top:10px;">
                            <div class="col-sm-1">
                                <img src="https://ca.slack-edge.com/T5XRADDQV-UFJBFBLCW-gfe6adacd859-48" height="50" width="50" class="img-thumbnail">
                            </div>
                            <div class="col-sm-11" style="padding-left:0px; margin-left:-14px;">
                                <div class="row>
                                    <div class="col">
                                        <b>${dataArr[i].username}
                                    </div>  
                                    <div id ="message-text"class="col">
                                        </b>${dataArr[i].text}<br>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        `
        }
        document.getElementById("message").innerHTML = message
    })

    channelHeadActive(channelId)
}

//=============CREATE MESSAGES================
function createMessage(event){
    event.preventDefault();
    let username = userName;
    let text = document.getElementById('messageText').value;
    let channel_id = channelId.id;

    console.log(channel_id)

    fetch('http://0.0.0.0:5000/broadcast', {
        method: 'POST',
       headers : new Headers(),
       body:JSON.stringify({username:username,text:text,channel_id:channel_id})
   })
   .then((data) =>  console.log(data))
   .catch((err)=>console.log(err))


    fetch('http://0.0.0.0:5000/messages/', {
        method: 'POST',
       headers : new Headers(),
       body:JSON.stringify({username:username,text:text,channel_id:channel_id})
   })
   .then((data) =>  console.log(data))
   .catch((err)=>console.log(err))  


   document.getElementById("createMessage").reset();

}


//=========== PUSHER ==========================
Pusher.logToConsole = true;

var pusher = new Pusher('703f0bf40a79092a845d', {
  cluster: 'ap2',
  forceTLS: true
});

var channel = pusher.subscribe('messages');
channel.bind('message-added', function(data) {
//   alert(JSON.stringify(data.text));
  console.log(data.username, data.text, data.channel_id)

        if(channelId.id === data.channel_id){

            message +=`<div class="row" style="margin-top:10px;">
                            <div class="col-sm-1">
                                <img src="https://ca.slack-edge.com/T5XRADDQV-UFJBFBLCW-gfe6adacd859-48" height="50" width="50" class="img-thumbnail">
                            </div>
                            <div class="col-sm-11" style="padding-left:0px; margin-left:-14px;">
                                <div class="row>
                                    <div class="col">
                                        <b>${data.username}
                                    </div>  
                                    <div id ="message-text"class="col">
                                        </b>${data.text}<br>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        `
        document.getElementById("pushMessage").innerHTML = message
    }
});

//====================CREATE CHANNEL============================
function createChannel(event){
     event.preventDefault();

     let name = document.getElementById('channel-name').value;

     fetch('http://0.0.0.0:5000/channels/', {
         method: 'POST',
        headers : new Headers(),
        body:JSON.stringify({name:name})
    }).then((res) => res.json())
    .then((data) =>  console.log(data))
    .catch((err)=>console.log(err))

    document.getElementById("createChannel").reset();

    document.getElementById("updateChannel").innerHTML = '#'+name;
}

//====================== SHOW CURRENT CHANNEL=============
function channelHeadActive(channelId){
    console.log(channelId.id)
    channelId = channelId.id;
    let url ='http://0.0.0.0:5000'
    fetch(url+'/channels?id='+channelId)
    .then(function(response) {
        console.log(response)
        return response.json();
        
    })
    .then(function(result){
        // console.log(result)
        let dataArr = result.resources;
        let name = dataArr[0].name
        document.getElementById('channel-head-active').innerHTML = name

    })

}



